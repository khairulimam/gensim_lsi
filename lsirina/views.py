import datetime
import time

from django.http import Http404
from django.shortcuts import render
from mongoengine.queryset import DoesNotExist

from lsi.lsi import LSI
from lsirina.models import Books


def index(request):
    query = request.GET.get('q')
    if not query:
        return render(request, 'index.html')
    time_start = time.time()
    books = Books.objects
    documents = [book for book in books]
    similarity = LSI([book['keterangan_preprocessed'].split() for book in books])
    found = similarity.similarity(query)
    sim = [round(x * 100, 2) for x in found]
    time_end = time.time()
    sort_by_most_valid = filter(lambda x: x[1] > 0, sorted(enumerate(sim), key=lambda item: -item[1]))
    detail_result = "Ditemukan %d hasil yang cocok dari %d dokumen dalam waktu %s detik" % (
        len(sort_by_most_valid), len(documents), datetime.timedelta(seconds=time_end - time_start).total_seconds())
    data = zip([documents[sort[0]] for sort in sort_by_most_valid], sort_by_most_valid)
    return render(request, 'search.html', {'sim': data, 'q': query, 'detail_result': detail_result, 'data_source': zip(books, found)})


def show_book(request, book_id):
    try:
        book = Books.objects.get(id=book_id)
        return render(request, 'show_book.html', {'book': book})
    except DoesNotExist:
        raise Http404("Tidak ada buku")


def login(request):
    pass
