from django.conf.urls import url

from . import views

urlpatterns = [
    url(r'^$', views.index, name='index'),
    url(r'^\w{1,50}/$', views.index, name='query_index'),
    url(r'^book/(?P<book_id>[a-zA-Z0-9]+)/$', views.show_book, name='show_book'),
    url(r'^login/$', views.login, name='login')
]
