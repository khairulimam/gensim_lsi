from django.conf.urls import url

import views

urlpatterns = [
    url(r'^$', views.index, name='admin.index'),
    url(r'^pengaturan/$', views.pengaturan, name='admin.pengaturan'),
    url(r'^keluar/$', views.logout, name='admin.logout'),
    # book urls
    url(r'^books/$', views.books, name='admin.books_index'),
    url(r'^book/(?P<book_id>[a-zA-Z0-9]+)/$', views.show_book, name='admin.book_show'),
    url(r'^addBook/$', views.add_book, name='admin.book_add'),
    url(r'^saveBook/$', views.save_book, name='admin.book_save'),
    url(r'^editBook/(?P<book_id>[a-zA-Z0-9]+)/$', views.edit_book, name='admin.book_edit'),
    url(r'^updateBook/$', views.update_book, name='admin.book_update'),
    url(r'^deleteBook/(?P<book_id>[a-zA-Z0-9]+)/$', views.delete_book, name='admin.book_delete'),
    url(r'^testQuery/$', views.test_query, name='admin.test_query'),
    url(r'^testBook/$', views.test_book, name='admin.test_book'),
    url(r'^performTestQuery/$', views.perform_test_query, name='admin.perform_test_query'),
    url(r'^performTestBook/$', views.perform_test_book, name='admin.perform_test_book'),
    url(r'^logBook/$', views.log_book, name='admin.log_book'),
    url(r'^logQuery/$', views.log_query, name='admin.log_query'),
    url(r'^showLogQuery/\w{1,50}/$', views.show_log_query, name='admin.show_log_query'),
    url(r'^showLogBook/\w{1,50}/$', views.show_log_book, name='admin.show_log_book'),
    url(r'^logSVD/$', views.show_log_svd, name='admin.show_log_svd'),
    url(r'^untestedBooks', views.show_untested, name='admin.show_untested_books')
]
