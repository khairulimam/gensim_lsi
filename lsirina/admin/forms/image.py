import uuid

from utils.utils import get_ext_from_img_header


class ImageForm:
    def __init__(self, file):
        self.file = file

    def is_img(self):
        return not get_ext_from_img_header(self.file.read()) is None

    def get_unique_name(self):
        return "%s.%s" % (uuid.uuid4(), self.file.name.split('.')[-1])
