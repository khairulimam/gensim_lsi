import datetime
import os
import time
from collections import Counter

from django.core.urlresolvers import reverse
from django.http import HttpResponse
from django.http import JsonResponse
from django.shortcuts import render
from docx import Document
from docx.enum.section import WD_ORIENTATION
from docx.shared import Cm, Mm
from texttable import Texttable

from gensimlsi import settings
from lsirina.admin.forms.book import AddBookForm, EditBookForm
from lsirina.admin.forms.image import ImageForm
from lsirina.lsi.accuration_score import precision_score, recall_score, f1_score
from lsirina.lsi.classification import Classification
from lsirina.lsi.lsi import LSI
from lsirina.lsi.svd import svd
from lsirina.models import Books
from utils.utils import extractErrorMessagesFromForm


def index(request):
    return render(request, 'auth/index.html')


def pengaturan(request):
    pass


def logout(request):
    pass


def books(request):
    books = Books.objects
    return render(request, 'auth/books/index.html', {'books': books})


def show_book(request, book_id):
    book = Books.objects.get(id=book_id)
    return render(request, 'auth/books/show.html', {'book': book})


def add_book(request):
    return render(request, 'auth/books/add.html')


def save_book(request):
    book_form = AddBookForm(request.POST)
    cover_file = request.FILES.get('cover')
    image_form = ImageForm(cover_file)
    is_img = True
    cover = settings.COVER_DEFAULT
    if cover_file:
        is_img = image_form.is_img()
        cover = image_form.get_unique_name()

    if book_form.validate() and is_img:
        new_book = Books()
        new_book.judul = book_form.judul.data
        new_book.penulis = settings.DEFAULT_PENULIS
        new_book.tahunTerbit = book_form.tahunTerbit.data
        new_book.kodeRak = book_form.kodeRak.data
        new_book.keterangan_ = book_form.keterangan.data
        new_book.cover = cover
        if cover_file:
            new_book.save_cover(cover_file)
        new_book.save()
        perform_log_svd()
        resp = {'status': 1, 'message': 'Berhasil menambah buku'}
    else:
        resp = {'status': 0, 'message': []}
        resp['message'].extend(extractErrorMessagesFromForm(book_form))
        if not is_img:
            resp['message'].extend(['Gambar tidak valid!'])
    return JsonResponse(resp)


def edit_book(request, book_id):
    book = Books.objects.get(id=book_id)
    return render(request, 'auth/books/edit.html', {'book': book})


def update_book(request):
    book_form = EditBookForm(request.POST)
    update_book = Books.objects.get(id=book_form.bookid.data)
    cover_file = request.FILES.get('cover')
    image_form = ImageForm(cover_file)
    is_img = True
    if cover_file:
        is_img = image_form.is_img()
        cover = image_form.get_unique_name()

    if book_form.validate() and is_img:
        update_book.judul = book_form.judul.data
        update_book.penulis = settings.DEFAULT_PENULIS
        update_book.tahunTerbit = book_form.tahunTerbit.data
        update_book.kodeRak = book_form.kodeRak.data
        update_book.keterangan_ = book_form.keterangan.data.encode('ascii', 'ignore')
        if cover_file:
            if update_book.cover == settings.COVER_DEFAULT:
                update_book.cover = cover
                update_book.save_cover(cover_file)
            else:
                update_book.save_cover(cover_file)
        update_book.save()
        perform_log_svd()
        resp = {'status': 1, 'message': 'Berhasil mengubah buku'}
    else:
        resp = {'status': 0, 'message': []}
        resp['message'].extend(extractErrorMessagesFromForm(book_form))
        if not is_img:
            resp['message'].extend(['Gambar tidak valid!'])
    return JsonResponse(resp)


def perform_log_svd():
    books = Books.objects
    keterangan_preprocesseds = map(lambda x: x.keterangan_preprocessed.lower(), books)
    U, S, Vt = svd(keterangan_preprocesseds)
    import numpy
    numpy.set_printoptions(precision=10, threshold='nan')
    svdlog = "Matrix U {}\n" \
             "{}\n" \
             "Matrix S {}\n" \
             "{}\n" \
             "Matrix V Transpose {}\n" \
             "{}\n".format(U.shape, str(U), S.shape, str(S), Vt.shape, str(Vt[:100]))
    with open(os.path.join(settings.LOG_SVD_PATH, settings.LOG_SVD_NAME), 'w+') as svd_log:
        svd_log.write(svdlog)


def delete_book(request, book_id):
    book = Books.objects.get(id=book_id)
    if not book.cover == settings.COVER_DEFAULT:
        os.remove(os.path.join(settings.COVER_PATH, book.cover))
    book.delete()
    return JsonResponse({'status': 1, 'message': 'Buku telah terhapus'})


def test_query(request):
    return render(request, 'auth/test/test_query.html')


def test_book(request):
    books = Books.objects
    return render(request, 'auth/test/test_book.html', {'books': books})


def perform_test_query(request):
    time_start = time.time()
    action = request.POST.get('action')
    query = request.POST.get('q')
    books = Books.objects
    documents = [book for book in books]
    similarity = LSI([book['keterangan_preprocessed'].split() for book in books])
    sim = similarity.similarity(query)
    filter_sim = filter(lambda x: x[1] > 0, enumerate(sim))
    data_source = [books[fse[0]] for fse in filter_sim]
    sort_by_most_valid = filter(lambda x: x[1] > 0, sorted(enumerate(sim), key=lambda item: -item[1]))
    data = zip([documents[sort[0]] for sort in sort_by_most_valid], sort_by_most_valid)
    if action == 'searchBooks':
        return JsonResponse({'found': [
            {'kodeBuku': item[0].kodeBuku, 'showBookUrl': reverse('show_book', kwargs={'book_id': str(item[0].id)})} for
            item in data[:settings.MAX_SHOW_SIM]], 'query': query,
            'data_source': zip(
                [{'judul': b.judul, 'kodeBuku_': b.kodeBuku_, 'kodeBuku': b.kodeBuku} for b in data_source],
                [str(s[1]) for s in filter_sim])})
    if action == 'testQuery':
        manual = [int(item) for item in request.POST.get('manual').split(',')]
        manual = set(manual)
        system = [int(item) for item in request.POST.get('system').split(',')]
        system = set(system)
        precision = precision_score(manual, system)
        recall = recall_score(manual, system)
        f1 = f1_score(precision, recall)
        table = Texttable()
        table.set_precision(10)
        table.set_cols_width([5, 10, 60, 15, 33])
        table.header(['No', 'Kode Buku', 'Judul', 'Nilai Kemiripan', 'URL'])
        for counter, item in enumerate(data, 1):
            table.add_row([counter, item[0].kodeBuku_, item[0].judul, item[1][1],
                           reverse('show_book', kwargs={'book_id': item[0].id})])
        text_table = table.draw()
        time_end = time.time()
        log = "Pengujian query selesai dalam waktu %s detik" \
              "\n\nQuery\t\t\t: %s \nSkor precision\t\t: %d%% \nSkor recall\t\t: %d%% \nSkor f1\t\t\t: %d%%" % (
                  datetime.timedelta(seconds=time_end - time_start).total_seconds(), query, round(precision * 100),
                  round(recall * 100), round(f1 * 100))
        log_with_text_table = "{} \n\nBuku yang cocok {}: \n{}".format(log, data.__len__(), text_table)
        logname = "{}.log".format(datetime.datetime.now())

        document = Document()
        section = document.sections[-1]
        table_grid_style = document.styles['Table Grid']
        section.orientation = WD_ORIENTATION.LANDSCAPE
        section.page_width = Mm(296)
        section.page_height = Mm(209)
        document.add_paragraph(log)
        document.add_paragraph("Buku yang cocok {}:".format(data.__len__()))
        word_table = document.add_table(rows=1, cols=4)
        word_table.style = table_grid_style
        hdr_cells = word_table.rows[0].cells
        hdr_cells[0].text = 'NO'
        word_table.columns[0].width = Cm(1)
        hdr_cells[1].text = 'Kode Buku'
        word_table.columns[1].width = Cm(2.5)
        hdr_cells[2].text = 'Judul'
        word_table.columns[2].width = Cm(16)
        hdr_cells[3].text = 'Nilai Kemiripan'
        word_table.columns[3].width = Cm(3.5)
        for counter, item in enumerate(data, 1):
            row_cells = word_table.add_row().cells
            row_cells[0].text = str(counter)
            row_cells[1].text = str(item[0].kodeBuku_)
            row_cells[2].text = str(item[0].judul)
            row_cells[3].text = str(item[1][1])
        document.save("{}.docx".format(os.path.join(settings.QUERY_LOG_PATH, logname)))
        with open(os.path.join(settings.QUERY_LOG_PATH, logname), 'w+') as query_log:
            query_log.write(log_with_text_table)
        url = "{}?log={}".format(reverse('admin.show_log_query'), logname)
        return JsonResponse({'scores': {'precision_score': round(precision * 100), 'recall_score': round(recall * 100),
                                        'f1_score': round(f1 * 100)}, 'url': url})


def perform_test_book(request):
    time_start = time.time()
    kodeBuku = request.POST.get('kodeBuku')
    bookToTest = Books.objects.get(kodeBuku=kodeBuku)
    table_freq = Texttable()
    table_freq.header(['No', 'Asal Kata', 'Frekuensi', 'TFIDF'])
    table_similaritas = Texttable()
    table_similaritas.set_cols_width([5, 10, 60, 15, 33])
    table_similaritas.header(['No', 'Kode Buku', 'Judul', 'Nilai Kemiripan', 'URL'])
    table_similaritas.set_cols_dtype(['i', 't', 't', 'a', 't'])
    table_similaritas.set_precision(10)
    table_freq.set_precision(10)
    books = Books.objects
    texts = [b['keterangan_preprocessed'].split() for b in books]
    similarity = LSI(texts)
    sim = similarity.calc_similarity(bookToTest.keterangan_preprocessed)
    freq_counter = Counter(bookToTest.keterangan_preprocessed.split())
    documents = [bookx for bookx in books]
    sort_by_most_valid = filter(lambda x: x[1] > 0, sorted(enumerate(sim), key=lambda item: -item[1]))[
                         1:settings.MAX_SHOW_SIM + 1]
    data = zip([documents[sort[0]] for sort in sort_by_most_valid], sort_by_most_valid)
    for counter, item in enumerate(data, 1):
        table_similaritas.add_row([counter, item[0].kodeBuku_, item[0].judul, item[1][1],
                                   reverse('show_book', kwargs={'book_id': item[0].id})])
    text_table_similaritas = table_similaritas.draw()
    for counter, item in enumerate(similarity.make_query_vector_tfidf(bookToTest.keterangan_preprocessed) , 1):
        term_ = similarity.dictionary[item[0]]
        table_freq.add_row([counter, term_, freq_counter[term_], item[1]])
    text_table_freq = table_freq.draw()
    classifier = Classification(data)
    classified_target = classifier.sort_score_by_highest()[0]
    if not bookToTest.kategori:
        bookToTest.kategori = classified_target[0]
        bookToTest.save()
    time_end = time.time()
    log = "Pengujian buku selesai dalam waktu {} detik" \
          "\n\nKode \t\t : {}\n" \
          "Judul \t\t : {}\n" \
          "Klasifikasi \t : {} dengan nilai confidence rata-rata {}\n" \
          "Keterangan \t : {}".format(datetime.timedelta(seconds=time_end - time_start).total_seconds(),
                                      bookToTest.kodeBuku_, bookToTest.judul,
                                      classified_target[0],
                                      classified_target[1] / data.__len__(),
                                      bookToTest.keterangan_clean)
    log_full = "{}\n" \
               "Nilai Frekuensi & TFIDF\n{}\n\n" \
               "Buku yang cocok {}\n{}".format(log, text_table_freq, data.__len__(), text_table_similaritas)
    logname = "{}.log".format(datetime.datetime.now())

    document = Document()
    section = document.sections[-1]
    table_grid_style = document.styles['Table Grid']
    section.orientation = WD_ORIENTATION.LANDSCAPE
    section.page_width = Mm(296)
    section.page_height = Mm(209)
    document.add_paragraph(log)
    document.add_paragraph("NIlai frekuensi & TFIDF")
    word_table_tfidf = document.add_table(rows=1, cols=4)
    word_table_tfidf.style = table_grid_style
    hdr_cells = word_table_tfidf.rows[0].cells
    hdr_cells[0].text = 'NO'
    word_table_tfidf.columns[0].width = Cm(1)
    hdr_cells[1].text = 'Asal Kata'
    word_table_tfidf.columns[1].width = Cm(4)
    hdr_cells[2].text = 'Frekuensi'
    word_table_tfidf.columns[2].width = Cm(3)
    hdr_cells[3].text = 'TFIDF'
    word_table_tfidf.columns[3].width = Cm(4)
    for counter, item in enumerate(similarity.make_query_vector_tfidf(bookToTest.keterangan_preprocessed), 1):
        term_ = similarity.dictionary[item[0]]
        row_cells = word_table_tfidf.add_row().cells
        row_cells[0].text = str(counter)
        row_cells[1].text = str(term_)
        row_cells[2].text = str(freq_counter[term_])
        row_cells[3].text = str(item[1])

    document.add_paragraph("\nBuku yang cocok {}:".format(data.__len__()))
    word_table_similarity = document.add_table(rows=1, cols=4)
    word_table_similarity.style = table_grid_style
    hdr_cells = word_table_similarity.rows[0].cells
    hdr_cells[0].text = 'NO'
    word_table_similarity.columns[0].width = Cm(1)
    hdr_cells[1].text = 'Kode Buku'
    word_table_similarity.columns[1].width = Cm(2.5)
    hdr_cells[2].text = 'Judul'
    word_table_similarity.columns[2].width = Cm(16)
    hdr_cells[3].text = 'Nilai Kemiripan'
    word_table_similarity.columns[3].width = Cm(3.5)
    for counter, item in enumerate(data, 1):
        row_cells = word_table_similarity.add_row().cells
        row_cells[0].text = str(counter)
        row_cells[1].text = str(item[0].kodeBuku_)
        row_cells[2].text = str(item[0].judul)
        row_cells[3].text = str(item[1][1])
    document.save("{}.docx".format(os.path.join(settings.BUKU_LOG_PATH, logname)))

    with open(os.path.join(settings.BUKU_LOG_PATH, logname), 'w+') as buku_log:
        buku_log.write(log_full)
    return JsonResponse({'url': "{}?log={}".format(reverse('admin.show_log_book'), logname)})


def log_book(request):
    logs = filter(lambda x: x.split('.')[-1] == 'log', os.listdir(settings.BUKU_LOG_PATH))
    return render(request, 'auth/bukulog.html', {'logs': logs})


def log_query(request):
    logs = filter(lambda x: x.split('.')[-1] == 'log', os.listdir(settings.QUERY_LOG_PATH))
    return render(request, 'auth/querylog.html', {'logs': logs})


def show_log_query(request):
    log = request.GET.get('log')
    with open(os.path.join(settings.QUERY_LOG_PATH, log), 'r') as flog:
        content = flog.read()
    return HttpResponse(content, content_type='text/plain')


def show_log_book(request):
    log = request.GET.get('log')
    with open(os.path.join(settings.BUKU_LOG_PATH, log), 'r') as blog:
        content = blog.read()
    return HttpResponse(content, content_type='text/plain')


def show_log_svd(request):
    with open(os.path.join(settings.LOG_SVD_PATH, settings.LOG_SVD_NAME), 'r') as svd_log:
        content = svd_log.read()
    return HttpResponse(content, content_type='text/plain')


def show_untested(request):
    books = Books.objects
    untested_book = filter(lambda x: not x.kategori, books)
    return JsonResponse({'untested': [b.kodeBuku for b in untested_book]})
