from sklearn.feature_extraction.text import TfidfVectorizer
from sklearn.utils.extmath import randomized_svd

from lsirina.lsi.stopwords import stop_word_models


def svd(texts):
    tfidf = tfidf_of(texts)
    return randomized_svd(tfidf, n_components=tfidf.shape[0])

def tfidf_of(texts):
    vectorizer = TfidfVectorizer(use_idf=True, ngram_range=(1, 3), stop_words=stop_word_models())
    TFIDF_sparse_matrix = vectorizer.fit_transform(texts)
    return TFIDF_sparse_matrix