class Classification:
    def __init__(self, class_data):
        self.data = class_data
        self.target = filter(lambda cat: not cat is None, [b[0].kategori for b in class_data])
        self.target_unique = set(self.target)

    def target_score(self):
        sum_target_score = list()
        for cat in self.target_unique:
            sum_target_score.append(
                (cat, sum(map(lambda x: x[1][1], filter(lambda y: y[0].kategori == cat, self.data)))))
        return sum_target_score

    def sort_score_by_highest(self):
        return sorted(self.target_score(), key=lambda item: -item[1])
