from gensim import similarities

from lsirina.lsi.indexing import make_dictionary, make_vector_space_from_dictionary, make_tfidf_model
from lsirina.lsi.preprocessing import stemmer


class LSI:
    def __init__(self, texts):
        self.dictionary = make_dictionary(texts)
        self.corpus = make_vector_space_from_dictionary(self.dictionary, texts)
        self.tfidf_model = make_tfidf_model(self.corpus)
        self.corpus_tfidf = self.tfidf_model[self.corpus]
        self.index = similarities.SparseMatrixSimilarity(self.corpus_tfidf, num_features=len(self.dictionary))

    def make_query_vector_tfidf(self, query):
        query_vec = self.dictionary.doc2bow(query.lower().split())
        query_vec_tfidf = self.tfidf_model[query_vec]
        return query_vec_tfidf

    def similarity(self, query):
        query = stemmer(query)
        return self.index[self.make_query_vector_tfidf(query)]

    def calc_similarity(self, query):
        return self.index[self.make_query_vector_tfidf(query)]
