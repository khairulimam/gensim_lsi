from gensim import corpora
from gensim import models


def make_dictionary(texts):
    return corpora.Dictionary(texts)


def make_vector_space_from_dictionary(dictionary, texts):
    return map(lambda text: dictionary.doc2bow(text), texts)


def make_tfidf_model(vector_space):
    return models.TfidfModel(vector_space)
