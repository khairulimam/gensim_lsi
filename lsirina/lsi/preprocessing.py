from Sastrawi.Stemmer.StemmerFactory import StemmerFactory

from stopwords import get_stop_word_free


def clean_stop_word(words):
    return get_stop_word_free(words)


def clean_and_stem(words):
    factory = StemmerFactory()
    stemmer = factory.create_stemmer()
    return [stemmer.stem(word) for word in clean_stop_word(words)]

def stemmer(text):
    factory = StemmerFactory()
    stemmer = factory.create_stemmer()
    return stemmer.stem(text)