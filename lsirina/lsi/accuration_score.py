def precision_score(manual, system):
    intersection = manual.intersection(system)
    return float(abs(intersection.__len__())) / float(abs(system.__len__())) * \
           float(abs(manual.__len__())) / float(abs(system.__len__()))


def recall_score(manual, system):
    intersection = manual.intersection(system)
    return float(abs(intersection.__len__())) / float(abs(system.__len__())) * \
           float(abs(manual.__len__())) / float(abs(manual.__len__()))


def f1_score(precision_score, recall_score):
    return (2 * precision_score * recall_score) / (precision_score + recall_score)
