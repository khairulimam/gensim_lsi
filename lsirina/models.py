import os

from mongoengine import *

from gensimlsi import settings
from lsirina.lsi.preprocessing import clean_and_stem
from utils.utils import clean_html

connect('lsi')


class BookCategories(Document):
    kategori = StringField(max_length=20)

    def __unicode__(self):
        return "Kategori %s" % self.kategori


class Books(Document):
    MAX_TEXT_LENGTH_TO_DISPLAY = 400

    kodeBuku = SequenceField(unique=True)
    judul = StringField()
    cover = StringField(max_length=50)
    kodeRak = StringField(max_length=10)
    tahunTerbit = StringField(max_length=4)
    penulis = StringField(max_length=50)
    keterangan = StringField()
    keterangan_clean = StringField()
    keterangan_preprocessed = StringField()
    kategori = StringField(max_length=20)

    @property
    def keterangan_(self):
        return self.keterangan

    @property
    def kodeBuku_(self):
        return "B%05d" % self.kodeBuku

    @keterangan_.setter
    def keterangan_(self, keterangan):
        self.keterangan = keterangan
        self.__keterangan_clean__()
        self.__keterangan_preprocessed__()

    def __keterangan_clean__(self):
        self.keterangan_clean = clean_html(self.keterangan)

    def __keterangan_preprocessed__(self):
        self.keterangan_preprocessed = str.join(' ', clean_and_stem(self.keterangan_clean))

    def get_splitted_keterangan(self):
        if len(self.keterangan_clean) > Books.MAX_TEXT_LENGTH_TO_DISPLAY:
            return "{}...".format(self.keterangan_clean[:Books.MAX_TEXT_LENGTH_TO_DISPLAY])
        return self.keterangan_clean

    def __unicode__(self):
        return "Kode: {}, Judul: {}, Penulis: {}".format(self.kodeBuku, self.judul, self.penulis)

    def save_cover(self, uploaded_cover):
        if self.cover == settings.COVER_DEFAULT:
            return
        with open(os.path.join(os.path.abspath(settings.COVER_PATH), self.cover), 'wb+') as f:
            for chunk in uploaded_cover.chunks():
                f.write(chunk)
