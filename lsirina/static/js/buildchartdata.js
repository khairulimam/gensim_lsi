/**
 * Created by khairulimam on 26/05/17.
 */

var chartData = {
    datasource: new Object(),
    label: function() {
        return this.datasource.map(function (currItem, index) {
            return currItem[0].kodeBuku_
        })
    },
    data: function () {
        return this.datasource.map(function (currItem, index) {
            return currItem[1]
        })
    },
    bubbleChartData: function () {
        return this.datasource.map(function (currItem) {
            radius = (currItem[1]*100)/100*25
            return {x: currItem[0].kodeBuku, y: currItem[1], r: Math.round(radius)+2}
        })
    }
}
